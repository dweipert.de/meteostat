<?php

namespace Meteostat\Tests;

use Meteostat\Client;
use Meteostat\Stations;
use Meteostat\Support\StationsMetaType;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class StationsTest extends TestCase
{
    private Stations $stations;

    public function setUp(): void
    {
        $client = new Client([], getenv('METEOSTAT_API_KEY'));

        $this->stations = $client->stations();
    }

    private function getData(ResponseInterface $response)
    {
        return json_decode((string)$response->getBody(), true)['data'];
    }

    public function testSearch()
    {
        $response = $this->stations->search('Sao Paulo');
        $data = $this->getData($response);

        $this->assertContains('83780', array_column($data, 'id'));
    }

    public function testNearby()
    {
        $response = $this->stations->nearby(-23.5505199, -46.6333094);
        $data = $this->getData($response);

        $this->assertContains('83779', array_column($data, 'id'));
    }

    public function testMeta()
    {
        $response = $this->stations->meta(StationsMetaType::ID, '83779');
        $data = $this->getData($response);

        $this->assertEquals('83779', $data['id']);
    }

    public function testHourly()
    {
        $response = $this->stations->hourly('83779', '2020-11-12', '2020-11-12');
        $data = $this->getData($response);

        $this->assertEquals(20, $data[0]['temp']);
    }

    public function testDaily()
    {
        $response = $this->stations->daily('83780', '2020-11-12', '2020-11-12');
        $data = $this->getData($response);

        $this->assertEquals(29.4, $data[0]['tmax']);
    }

    public function testClimate()
    {
        $response = $this->stations->climate('68816');
        $data = $this->getData($response);

        $this->assertEquals(26.1, $data[0]['tmax']);
    }
}
