<?php
namespace Meteostat\Tests;

use GuzzleHttp\ClientInterface;
use Meteostat\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    private ClientInterface $client;

    public function setUp(): void
    {
        $this->client = new Client([], getenv('METEOSTAT_API_KEY'));
    }

    public function testRequest()
    {
        $response = $this->client->request('GET', 'stations/meta', [
            'query' => [
                'id' => 10637,
            ],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
