<?php

namespace Meteostat\Tests;

use Meteostat\Client;
use Meteostat\Point;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class PointTest extends TestCase
{
    private Point $point;

    public function setUp(): void
    {
        $client = new Client([], getenv('METEOSTAT_API_KEY'));

        $this->point = $client->point();
    }

    private function getData(ResponseInterface $response)
    {
        return json_decode((string)$response->getBody(), true)['data'];
    }

    public function testHourly()
    {
        $response = $this->point->hourly(-23.5505199, -46.6333094, '2020-11-12', '2020-11-12');
        $data = $this->getData($response);

        $this->assertEquals(23.2, $data[0]['temp']);
    }

    public function testDaily()
    {
        $response = $this->point->daily(49.583332, 11.016667, '2019-06-01', '2019-06-30');
        $data = $this->getData($response);

        $this->assertEquals(29, $data[0]['tmax']);
    }

    public function testClimate()
    {
        $response = $this->point->climate(49.583332, 11.016667);
        $data = $this->getData($response);

        $this->assertEquals(4.3, $data[0]['tmax']);
    }
}
