PHP Wrapper for Meteostat JSON API
======

Check https://dev.meteostat.net/api/ for detailed API responses.

## Installation

```bash
composer require dweipert/meteostat
```

## Usage

### Instantiate a Client

```php
use Meteostat\Client;

$client = new Client([/* custom config */], 'METEOSTAT_API_KEY');
```

### Get the Stations wrapper

```php
$stations = $client->stations();
$response = $stations->search('Sao Paulo');
$body = json_decode((string)$response->getBody(), true);
```

JSON decoded response:

```
array(2) {
  ["meta"]=>
  array(2) {
    ["exec_time"]=>
    float(0.082)
    ["generated"]=>
    string(19) "2021-04-29 17:39:44"
  }
  ["data"]=>
  array(5) {
    [0]=>
    array(13) {
      ["id"]=>
      string(5) "83780"
      ["name"]=>
      array(1) {
        ["en"]=>
        string(29) "Sao Paulo/Congonhas Aeroporto"
      }
      ["country"]=>
      string(2) "BR"
      ["region"]=>
      string(2) "SP"
      ["national"]=>
      NULL
      ["wmo"]=>
      string(5) "83780"
      ["icao"]=>
      string(4) "SBSP"
      ["iata"]=>
      string(3) "CGH"
      ["latitude"]=>
      float(-23.6167)
      ["longitude"]=>
      float(-46.65)
      ["elevation"]=>
      int(803)
      ["timezone"]=>
      string(17) "America/Sao_Paulo"
      ["active"]=>
      bool(true)
    }
  [...]
  }
}
```

### Get the Point wrapper

```php
$point = $client->point();
$response = $point->climate(49.583332, 11.016667);
$body = json_decode((string)$response->getBody(), true);
```

JSON decoded response:

```
array(2) {
  ["meta"]=>
  array(5) {
    ["source"]=>
    string(9) "WorldClim"
    ["start"]=>
    int(1960)
    ["end"]=>
    int(1990)
    ["exec_time"]=>
    float(0.065)
    ["generated"]=>
    string(19) "2021-04-29 18:37:36"
  }
  ["data"]=>
  array(12) {
    [0]=>
    array(7) {
      ["month"]=>
      int(1)
      ["tavg"]=>
      float(1.5)
      ["tmin"]=>
      float(-1.3)
      ["tmax"]=>
      float(4.3)
      ["prcp"]=>
      int(44)
      ["pres"]=>
      NULL
      ["tsun"]=>
      NULL
    }
  [...]
  }
}
```
