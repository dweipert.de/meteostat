<?php

namespace Meteostat\Support;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

interface StationsInterface
{
    /**
     * StationsInterface constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client);

    /**
     * @see https://dev.meteostat.net/api/stations/search.html
     *
     * @param string $query
     * @param int $limit
     *
     * @return ResponseInterface
     */
    public function search(string $query, int $limit = 8): ResponseInterface;

    /**
     * @see https://dev.meteostat.net/api/stations/nearby.html
     *
     * @param float $lat
     * @param float $lon
     * @param int $limit
     * @param int $radius
     *
     * @return ResponseInterface
     */
    public function nearby(float $lat, float $lon, int $limit = 8, int $radius = 100): ResponseInterface;

    /**
     * @see https://dev.meteostat.net/api/stations/meta.html
     *
     * @param StationsMetaType|string $type
     * @param string|int $value
     *
     * @return ResponseInterface
     */
    public function meta(string $type, string|int $value): ResponseInterface;

    /**
     * @see https://dev.meteostat.net/api/stations/hourly.html
     *
     * @param string $station
     * @param string $start
     * @param string $end
     * @param string $tz
     * @param bool $model
     *
     * @return ResponseInterface
     */
    public function hourly(string $station, string $start, string $end, string $tz = 'UTC', bool $model = false): ResponseInterface;

    /**
     * @see https://dev.meteostat.net/api/stations/daily.html
     *
     * @param string $station
     * @param string $start
     * @param string $end
     *
     * @return ResponseInterface
     */
    public function daily(string $station, string $start, string $end): ResponseInterface;

    /**
     * @see https://dev.meteostat.net/api/stations/climate.html
     *
     * @param string $station
     *
     * @return ResponseInterface
     */
    public function climate(string $station): ResponseInterface;
}
