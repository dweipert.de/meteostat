<?php

namespace Meteostat\Support;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

interface PointInterface
{
    /**
     * PointInterface constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client);

    /**
     * @see https://dev.meteostat.net/api/point/hourly.html
     *
     * @param float $lat
     * @param float $lon
     * @param string $start
     * @param string $end
     * @param int $alt
     * @param string $tz
     *
     * @return ResponseInterface
     */
    public function hourly(float $lat, float $lon, string $start, string $end, int $alt = 0, string $tz = 'UTC'): ResponseInterface;

    /**
     * @see https://dev.meteostat.net/api/point/daily.html
     *
     * @param float $lat
     * @param float $lon
     * @param string $start
     * @param string $end
     * @param int $alt
     *
     * @return ResponseInterface
     */
    public function daily(float $lat, float $lon, string $start, string $end, int $alt = 0): ResponseInterface;

    /**
     * @see https://dev.meteostat.net/api/point/climate.html
     *
     * @param float $lat
     * @param float $lon
     * @param int $alt
     *
     * @return ResponseInterface
     */
    public function climate(float $lat, float $lon, int $alt = 0): ResponseInterface;
}
