<?php

namespace Meteostat\Support;

class StationsMetaType
{
    public const ID = 'id';
    public const WMO = 'wmo';
    public const ICAO = 'icao';
}
