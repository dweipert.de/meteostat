<?php

namespace Meteostat;

use GuzzleHttp\ClientInterface;
use Meteostat\Support\PointInterface;
use Psr\Http\Message\ResponseInterface;

class Point implements PointInterface
{
    /**
     * Point constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(private ClientInterface $client) {}

    /**
     * Wrapper to append "point/" to $uri
     *
     * @param string $method
     * @param string $uri
     * @param array $options
     *
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, string $uri, array $options = []): ResponseInterface
    {
        return $this->client->request($method, "point/$uri", $options);
    }

    /**
     * @inheritDoc
     */
    public function hourly(float $lat, float $lon, string $start, string $end, int $alt = 0, string $tz = 'UTC'): ResponseInterface
    {
        return $this->request('GET', 'hourly', [
            'query' => [
                'lat' => $lat,
                'lon' => $lon,
                'start' => $start,
                'end' => $end,
                'alt' => $alt,
                'tz' => $tz,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function daily(float $lat, float $lon, string $start, string $end, int $alt = 0): ResponseInterface
    {
        return $this->request('GET', 'daily', [
            'query' => [
                'lat' => $lat,
                'lon' => $lon,
                'start' => $start,
                'end' => $end,
                'alt' => $alt,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function climate(float $lat, float $lon, int $alt = 0): ResponseInterface
    {
        return $this->request('GET', 'climate', [
            'query' => [
                'lat' => $lat,
                'lon' => $lon,
                'alt' => $alt,
            ],
        ]);
    }
}
