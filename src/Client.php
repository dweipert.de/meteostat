<?php

namespace Meteostat;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Promise\PromiseInterface;
use Meteostat\Support\StationsInterface;
use Meteostat\Support\PointInterface;

class Client extends GuzzleClient
{
    private StationsInterface $stations;
    private PointInterface $point;

    /**
     * Client constructor.
     *
     * @param array $config
     * @param string $key
     */
    public function __construct(array $config, private string $key)
    {
        $config = array_replace([
            'base_uri' => 'https://api.meteostat.net/v2/',
        ], $config);

        parent::__construct($config);
    }

    /**
     * Overwrite to add API Key to every request
     *
     * @inheritDoc
     */
    public function requestAsync(string $method, $uri = '', array $options = []): PromiseInterface
    {
        $options['headers'] = array_merge($options['headers'] ?? [], ['x-api-key' => $this->key]);

        return parent::requestAsync($method, $uri, $options);
    }

    /**
     * @param string $stations
     *
     * @return StationsInterface
     */
    public function stations(string $stations = Stations::class): StationsInterface
    {
        return $this->stations ?? $this->stations = new $stations($this);
    }

    /**
     * @param string $point
     *
     * @return PointInterface
     */
    public function point(string $point = Point::class): PointInterface
    {
        return $this->point ?? $this->point = new $point($this);
    }
}
