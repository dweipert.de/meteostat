<?php

namespace Meteostat;

use GuzzleHttp\ClientInterface;
use Meteostat\Support\StationsInterface;
use Psr\Http\Message\ResponseInterface;

class Stations implements StationsInterface
{
    /**
     * Stations constructor.
     *
     * @param ClientInterface $client
     */
    public function __construct(private ClientInterface $client) {}

    /**
     * Wrapper to append "stations/" to $uri
     *
     * @param string $method
     * @param string $uri
     * @param array $options
     *
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, string $uri, array $options = []): ResponseInterface
    {
        return $this->client->request($method, "stations/$uri", $options);
    }

    /**
     * @inheritDoc
     */
    public function search(string $query, int $limit = 8): ResponseInterface
    {
        return $this->request('GET', 'search', [
            'query' => [
                'query' => $query,
                'limit' => $limit,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function nearby(float $lat, float $lon, int $limit = 8, int $radius = 100): ResponseInterface
    {
        return $this->request('GET', 'nearby', [
            'query' => [
                'lat' => $lat,
                'lon' => $lon,
                'limit' => $limit,
                'radius' => $radius,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function meta(string $type, int|string $value): ResponseInterface
    {
        return $this->request('GET', 'meta', [
            'query' => [
                $type => $value,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function hourly(string $station, string $start, string $end, string $tz = 'UTC', bool $model = false): ResponseInterface
    {
        return $this->request('GET', 'hourly', [
            'query' => [
                'station' => $station,
                'start' => $start,
                'end' => $end,
                'tz' => $tz,
                'model' => $model,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function daily(string $station, string $start, string $end): ResponseInterface
    {
        return $this->request('GET', 'daily', [
            'query' => [
                'station' => $station,
                'start' => $start,
                'end' => $end,
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function climate(string $station): ResponseInterface
    {
        return $this->request('GET', 'climate', [
            'query' => [
                'station' => $station,
            ],
        ]);
    }
}
